# 注意
Entity里提示报错不是缺少get、set方法，Eclipse、IDEA请先安装lombok插件


# 官网
[https://mallti.wxngrok.com](https://mallti.wxngrok.com)

- 标准商业版gitee仓库：https://gitee.com/fuyang_lipengjun/platform
- 供应商版,请微信联系


# 使用须知
## 基于微同商城的改造版本 
- 如需供应商版本请微信联系

![](http://www.wxngrok.com/mallti/qrcode.jpg "微信")

# 新增功能
由惠州市酷天科技基于微同商城二次开发的新增功能

在标准商业版基础上拓展了供应商模块的功能
- 修复商城bug    
- 供应商入驻
- 供应商管理
    - 订单结算
    - 供应商发货
    - 供应商退货
- 财务管理
    - 供应商结算
    - 财务对账记录
- 礼品卡模块
    礼品卡生成和发放
- 组合支付模块
    - 微信支付
    - 积分支付
    - 余额支付
- 商品管理
    - 多小区铺货
- 订单管理
    - 多供应商订单拆分结算
    - 自提订单
    - 快递订单
    - 送货上门,订单核销
    
- 技术讨论、二次开发等咨询、问题和建议，请加微信！

# 微信小程序商城（Java版）

## 新手必看启动教程
- [https://www.bilibili.com/video/av66149752](https://www.bilibili.com/video/av66149752)
## 使用Hbuilder启动微同商城小程序端教程
- [https://www.bilibili.com/video/BV1ni4y1M7CC](https://www.bilibili.com/video/BV1ni4y1M7CC)

## 技术选型
* 1 后端使用技术
    * 1.1 springframework4.3.7.RELEASE
    * 1.2 mybatis3.1.0、MyBatis-Plus 3.1.0
    * 1.3 shiro1.3.2
    * 1.4 servlet3.1.0
    * 1.5 druid1.0.28
    * 1.6 slf4j1.7.19
    * 1.7 fastjson1.2.30
    * 1.8 poi3.15
    * 1.9 velocity1.7
    * 1.10 quartz2.2.3
    * 1.11 mysql5.1.39
    * 1.12 swagger2.4
    * 1.13 j2cache2.3.22-release
    * 1.14 weixin-java-mp3.2.0
    * 1.15 MybatisPlus3.1.0
    * 1.16 lombok
        
* 2 前端使用技术
    * 2.1 Vue2.5.1
    * 2.2 iview
    * 2.3 layer3.0.3
    * 2.4 jquery2.2.4
    * 2.5 bootstrap3.3.7
    * 2.6 jqgrid5.1.1
    * 2.7 ztree3.5.26
    * 2.8 froala_editor1.2.2

## 项目结构
~~~
platform
|--platform-admin 后台管理
|--platform-api 微信小程序商城api接口
|--platform-common 公共模块
|--platform-framework 系统WEB合并，请打包发布此项目
|--platform-gen 代码生成
|--platform-mp 微信公众号模块
|--platform-schedule 定时任务
|--platform-shop 商城后台管理
|--uni-mall uniapp版商城
|--wx-mall 微信小程序原生商城
~~~

## 实现功能

* 一：会员管理
    * a 会员管理
    * b 会员等级
    * c 收货地址管理
    * d 会员优惠劵
    * e 会员收藏
    * f 会员足迹
    * g 搜索历史
    * h 购物车

* 二：商城配置
    * a 区域配置
    * b 商品属性种类
    * c 品牌制造商
    * d 商品规格
    * e 订单管理
    * f 商品类型
    * g 渠道管理
    * h 商品问答
    * i 反馈
    * j 关键词

* 三：商品编辑
    * a 所有商品
    * b 用户评论
    * c 产品设置
    * d 商品规格
    * e 商品回收站

* 四：推广管理
    * a 广告列表
    * b 广告位置
    * c 优惠劵管理
    * d 专题管理
    * e 专题分类

* 五：订单管理
    * a 所有订单管理

* 六：系统管理
    * a 管理员列表
    * b 角色管理
    * c 菜单管理
    * d SQL监控
    * e 定时任务
    * f 参数管理
    * g 代码生成器
    * h 系统日志
    * i 文件上传
    * j 通用字典表
        
* 七：短信服务平台  
    * a 配置短信平台账户信息
    * b 向外提供发送短信接口：
    ```
    http://域名:端口/api/sendSms?mobile=13000000000,15209831990&content=发送的短信内容  
    安全起见，需配置有效IP地址。platform.properties -> sms.validIp
    ```

## 安装教程

* 配置环境（推荐jdk1.8、maven3.3、tomcat8、mysql5.7、redis4.0.1）
* 创建数据库
* 依次初始化sql脚本 
    * /_sql/platform.sql
    * /_sql/sys_region.sql
* 导入项目到IDE中
* 导入支付证书至/platform-shop/src/main/resources/cert/目录下（申请商户号、开通微信支付、下载支付证书）
* 修改配置文件 /platform-admin/src/main/resources/dev/platform.properties
    * jdbc.url
    * jdbc.username
    * jdbc.password
    * wx.appId
    * wx.secret
    * wx.mchId
    * wx.paySignKey
    * wx.notifyUrl
    * sms.validIp
    * mp.appId
    * mp.secret
    * mp.token
    * mp.aesKey
* 修改配置文件 /platform-admin/src/main/resources/j2cache.properties
    * redis.hosts
    * redis.password
* 启动redis服务
* 启动后台项目（参照<a href="#doc">开发文档</a>）
* 打开微信开发者工具
* 导入 /wx-mall填写appId
* 修改 /wx-mall/config/api.js里API_BASE_URL的值
* 使用eclipse启动项目后默认访问路径
    * [http://localhost:8080/platform-framework](http://localhost:8080/platform-framework)
* 使用idea启动项目后默认访问路径
    * [http://localhost:8080](http://localhost:8080)

## 页面展示
### 系统功能模块
![](http://www.wxngrok.com/mallti/系统功能模块.png "系统功能模块") 
### 系统功能模块
![](http://www.wxngrok.com/mallti/供应商模块.png "供应商模块")
### 供应商入驻
![](http://www.wxngrok.com/mallti/供应商入驻.png "供应商入驻")
### 礼品卡模块
![](http://www.wxngrok.com/mallti/礼品卡模块.png "礼品卡模块")

### 小程序小区选择,多小区经营,一小区一店铺模式
![](http://www.wxngrok.com/mallti/小程序小区选择.png "分类")
### 小程序首页
![](http://www.wxngrok.com/mallti/小程序首页.png "小程序首页")
### 分类
![](http://www.wxngrok.com/mallti/小程序分类.png "分类")
### 购物车：支持快递、送货上门、自提
![](http://www.wxngrok.com/mallti/小程序购物车.png "购物车")
### 礼品卡：支持发放实物礼品卡，线上线下结合的经营模式
![](http://www.wxngrok.com/mallti/小程序礼品卡领取.png "礼品卡")
### 组合支付：微信支付，积分支付，充值余额支付，礼品卡支付
![](http://www.wxngrok.com/mallti/小程序组合支付.png "组合支付")
### 个人中心
![](http://www.wxngrok.com/mallti/小程序个人中心.png "个人中心")

### 优惠券
![](http://www.wxngrok.com/mallti/小程序优惠券.png "优惠券")

### 开发文档目录
<a name="doc" href="http://fly2you.cn/doc.html" target="_blank">![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/catalog.png "开发文档目录")</a>